<?php

use Alikhani\Payment\Drivers\Behpardakht;
use Alikhani\Payment\Drivers\Saman;

return [
    /*
    |--------------------------------------------------------------------------
    | Default Driver
    |--------------------------------------------------------------------------
    |
    | This value determines which of the following gateway to use.
    | You can switch to a different driver at runtime.
    |
    */
    'default' => 'behpardakht',

    /*
    |--------------------------------------------------------------------------
    | List of Drivers
    |--------------------------------------------------------------------------
    |
    | These are the list of drivers to use for this package.
    | You can change the name. Then you'll have to change
    | it in the map array too.
    |
    */
    'drivers' => [
        'behpardakht' => [
            'gateway'            => Behpardakht::class,
            'apiPurchaseUrl'     => 'https://bpm.shaparak.ir/pgwchannel/services/pgw?wsdl',
            'apiPaymentUrl'      => 'https://bpm.shaparak.ir/pgwchannel/startpay.mellat',
            'apiVerificationUrl' => 'https://bpm.shaparak.ir/pgwchannel/services/pgw?wsdl',
            'apiRefundUrl'       => 'https://bpm.shaparak.ir/pgwchannel/services/pgw?wsdl',
            'description'        => 'payment using behpardakht',
            'provider_code'      => 10000
        ],
        'saman'       => [
            'gateway'            => Saman::class,
            'apiPurchaseUrl'     => 'https://sep.shaparak.ir/Payments/InitPayment.asmx?WSDL',
            'apiPaymentUrl'      => 'https://sep.shaparak.ir/payment.aspx',
            'apiVerificationUrl' => 'https://sep.shaparak.ir/payments/referencepayment.asmx?WSDL',
            'apiRefundUrl'       => 'https://srtm.sep.ir/RefundService/srvRefundV2.svc?wsdl',
            'description'        => 'payment using saman',
        ],
    ],
];
