<?php

namespace Alikhani\Payment;

use App\Services\PaymentService\V1\Enumerations\Payment\Type;
use Exception;
use InvalidArgumentException;
use Alikhani\Payment\Contracts\DriverInterface;
use Alikhani\Payment\Exceptions\DriverNotFoundException;
use Alikhani\Payment\Traits\Detailable;

/**
 * @property string $id
 * @property array $config
 * @property int $amount
 * @property string $transactionId
 * @property string $driver
 */
class Payment
{
    use Detailable;

    protected string $id;

    /**
     * Amount
     *
     * @var int
     */
    protected int $amount = 0;

    /**
     * payment transaction id
     *
     * @var string
     */
    protected string $transactionId;

    /**
     * @var string
     */
    protected string $driver;

    /**
     * @var object
     */
    protected object $config;

    protected Type $type;

    /**
     * Retrieve given value from details
     *
     * @param $name
     * @return mixed
     */
    public function __get($name)
    {
        return $this?->$name;
    }

    /**
     * @param string $id
     * @return $this
     */
    public function id(string $id): static
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Set the amount of invoice
     *
     * @param int $amount
     * @return $this
     * @throws InvalidArgumentException
     */
    public function amount(int $amount): static
    {
        if (!is_numeric($amount)) {
            throw new InvalidArgumentException('Amount value should be a number.');
        }
        $this->amount = $amount;

        return $this;
    }

    /**
     * set transaction id
     *
     * @param string $id
     * @return $this
     */
    public function transactionId(string $id): static
    {
        $this->transactionId = $id;

        return $this;
    }

    public function type(Type $type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Set the value of driver
     *
     * @param string $driver
     * @return $this
     * @throws Exception
     */
    public function via(array $config, string $driver): static
    {
        $this->loadConfig($config, $driver);

        return $this;
    }

    /**
     * @throws Exception
     */
    public function loadConfig(array $config, string $driver)
    {
        $config = array_merge($config, config("payment.drivers.$driver"));

        $this->driver = $driver;
        $this->config = (object)$config;
    }

    /**
     * @param string $url
     * @return Payment
     */
    public function callback(string $url): static
    {
        $this->config->callbackUrl = $url;
        return $this;
    }

    /**
     * @return DriverInterface
     */
    public function gateway(): DriverInterface
    {
        $gateway = $this->config->gateway;

        return new $gateway($this);
    }
}
