<?php
namespace Alikhani\Payment\Facades;

use Illuminate\Support\Facades\Facade;
use Alikhani\Payment\Contracts\DriverInterface;

/**
 * @method static \Alikhani\Payment\Payment id(string $id)
 * @method static \Alikhani\Payment\Payment amount(int $amount)
 * @method static \Alikhani\Payment\Payment transactionId(string $id)
 * @method static \Alikhani\Payment\Payment via(array $config, string $driver)
 * @method static \Alikhani\Payment\Payment callback(string $url)
 * @method DriverInterface gateway()
 */
class Payment extends Facade
{
    protected static function getFacadeAccessor(): string
    {
        return 'payment';
    }
}
