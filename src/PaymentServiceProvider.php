<?php

namespace Alikhani\Payment;

use Illuminate\Support\ServiceProvider;

class PaymentServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $this->app->bind('payment', function($app) {
            return new Payment($app);
        });

        $this->mergeConfigFrom(__DIR__ . '/../config/payment.php', 'payment');
        /**
         * Configurations that needs to be done by user.
         */
        $this->publishes(
            [
                __DIR__ . '/../config/payment.php' => config_path('payment.php'),
            ],
            'config-gateway'
        );

        $this->loadViewsFrom([__DIR__ . '/../resources/views'], 'payment');

    }

    public function register()
    {
        //
    }
}
